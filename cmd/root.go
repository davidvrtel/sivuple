package cmd

import (
	"github.com/spf13/cobra"
)

// output type variable used by subcommands
var outputType string

var rootCmd = &cobra.Command{
	Use:   "sivuple",
	Short: "SiVuPle is opinionated Semantic Version Parser",
	Long: `SiVuPle, opinionated Semantic Version Parser.

Simantic Vursion Plerser.
More info at https://gitlab.com/davidvrtel/sivuple.`,
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		return cmd.Help()
	},
}

func Execute() error {
	return rootCmd.Execute()
}
