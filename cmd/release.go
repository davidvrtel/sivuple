package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/davidvrtel/sivuple/pkg"
)

var releaseCmd = &cobra.Command{
	Use:                   "release {major|minor|patch} <version> [-o text|json|yaml]",
	Aliases:               []string{"r"},
	Short:                 "Raise version by type",
	Long:                  `Raise version to create new release, specify type: major|minor|patch`,
	Args:                  cobra.ExactArgs(2),
	DisableFlagsInUseLine: true,
	RunE:                  runRelease,
}

func runRelease(cmd *cobra.Command, args []string) error {
	outputType, err := pkg.ParseOutputType(outputType)
	if err != nil {
		return err
	}

	versionType, err := pkg.ParseVersionType(args[0])
	if err != nil {
		return err
	}

	raisedVersion, err := pkg.RaiseCore(args[1], versionType)
	if err != nil {
		return err
	}

	output, err := pkg.FormatOutput(raisedVersion, outputType)
	if err != nil {
		return err
	}

	fmt.Println(output)
	return nil
}

// built in function name for additional configuration
func init() {
	releaseCmd.Flags().StringVarP(&outputType, "output", "o", string(pkg.Text), "specify output type: text|json|yaml")
	rootCmd.AddCommand(releaseCmd)
}
