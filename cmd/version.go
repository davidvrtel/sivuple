package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:     "version",
	Aliases: []string{"V"},
	Short:   "Print the version number of SiVuPle",
	Long:    `All software has versions. This is SiVuPle's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Simantic Vursion Plerser v0.9 -- HEAD")
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
