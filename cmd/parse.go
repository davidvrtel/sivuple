package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/davidvrtel/sivuple/pkg"
)

var parseCmd = &cobra.Command{
	Use:                   "parse <version> [-o text|json|yaml]",
	Aliases:               []string{"p"},
	Short:                 "Parse version",
	Long:                  "Parse version provided as argument.",
	Args:                  cobra.ExactArgs(1),
	DisableFlagsInUseLine: true,
	RunE:                  runParse,
}

func runParse(cmd *cobra.Command, args []string) error {
	outputType, err := pkg.ParseOutputType(outputType)
	if err != nil {
		return err
	}

	parsedVersion, err := pkg.ParseVersion(args[0])
	if err != nil {
		return err
	}

	output, err := pkg.FormatOutput(parsedVersion, outputType)
	if err != nil {
		return err
	}

	fmt.Println(output)
	return nil
}

// built in function name for additional configuration
func init() {
	parseCmd.Flags().StringVarP(&outputType, "output", "o", string(pkg.Text), "specify output type: text|json|yaml")
	rootCmd.AddCommand(parseCmd)
}
