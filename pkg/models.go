package pkg

type VersionType string

const (
	Major VersionType = "MAJOR"
	Minor VersionType = "MINOR"
	Patch VersionType = "PATCH"
)

var coreVersionsTypeMap = map[string]VersionType{
	"MAJOR": Major,
	"MINOR": Minor,
	"PATCH": Patch,
}

var coreVersionsPositionMap = map[int]string{
	0: "MAJOR",
	1: "MINOR",
	2: "PATCH",
}

// Output type helper enum
type OutputType string

const (
	Text OutputType = "text"
	Json OutputType = "json"
	Yaml OutputType = "yaml"
)

var outputTypeMap = map[string]OutputType{
	"text": Text,
	"json": Json,
	"yaml": Yaml,
}

// intermediate parsed struct
type splitVersion struct {
	core          string
	hasPreRelease bool
	preRelease    string
	hasMetadata   bool
	metadata      string
}

// final parsed struct
type ParsedVersion struct {
	Major            uint     `json:"major" yaml:"major"`
	Minor            uint     `json:"minor" yaml:"minor"`
	Patch            uint     `json:"patch" yaml:"patch"`
	PreReleaseFields []string `yaml,json:"preReleaseFields" yaml:"preReleaseFields"`
	MetadataFields   []string `json:"metadatFields" yaml:"metadatFields"`
}

func (v *ParsedVersion) raiseMajor() {
	v.Major++
	v.Minor = 0
	v.Patch = 0
}

func (v *ParsedVersion) raiseMinor() {
	v.Minor++
	v.Patch = 0
}

func (v *ParsedVersion) raisePatch() {
	v.Patch++
}
