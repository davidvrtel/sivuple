package pkg

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var customRegexp = regexp.MustCompile("^[a-zA-Z0-9-]*$")

func isAlphaNumericHyphen(arg string) bool {
	return customRegexp.MatchString(arg)
}

func validateOutputType(arg string) error {
	_, ok := outputTypeMap[strings.ToLower(arg)]
	if !ok {
		return fmt.Errorf("%v is not valid output type, expected text|json|yaml", arg)
	}
	return nil
}

func validateVersionType(arg string) error {
	_, ok := coreVersionsTypeMap[strings.ToUpper(arg)]
	if !ok {
		return fmt.Errorf("%v is not valid version type, expected major|minor|patch", arg)
	}
	return nil
}

func validateCore(arg string) error {
	stringArray := strings.Split(arg, ".")

	if len(stringArray) != 3 {
		return fmt.Errorf("invalid SemVer core (value: %v): cannot find MAJOR, MINOR and PATCH versions", arg)
	}

	for i, v := range stringArray {
		_, err := strconv.ParseUint(v, 10, 0)

		if err != nil || (len(v) > 1 && v[0:1] == "0") {
			return fmt.Errorf("invalid SemVer core (value: %v), %v version: must be non-negative integer, must not contain leading zeroes", arg, coreVersionsPositionMap[i])
		}

	}
	return nil
}

func validatePreRelease(arg string) error {
	if len(arg) == 0 {
		return fmt.Errorf("invalid SemVer pre release (value: %v): must not be empty", arg)
	}

	stringArray := strings.Split(arg, ".")

	for _, v := range stringArray {
		if len(v) == 0 {
			return fmt.Errorf("invalid SemVer pre release (value: %v): found empty identifier", arg)
		}

		if !isAlphaNumericHyphen(v) {
			return fmt.Errorf("invalid SemVer pre release (value: %v): identifier %v does comprise only [0-9A-Za-z-]", arg, v)
		}

		_, err := strconv.ParseUint(v, 10, 0)

		if err == nil && v[0:1] == "0" {
			return fmt.Errorf("invalid SemVer pre release (value: %v): identifier %v is numeric and starts with zero", arg, v)
		}
	}
	return nil
}

func validateMetadata(arg string) error {
	if len(arg) == 0 {
		return fmt.Errorf("invalid SemVer metadata (value: %v): must not be empty", arg)
	}

	stringArray := strings.Split(arg, ".")

	for _, v := range stringArray {
		if len(v) == 0 {
			return fmt.Errorf("invalid SemVer metadata (value: %v): found empty identifier", arg)
		}

		if !isAlphaNumericHyphen(v) {
			return fmt.Errorf("invalid SemVer metadata (value: %v): identifier %v does comprise only [0-9A-Za-z-]", arg, v)
		}
	}
	return nil
}
