package pkg

import "fmt"

func RaiseCore(version string, versionType VersionType) (ParsedVersion, error) {
	parsedVersion, err := ParseVersion(version)

	if err != nil {
		return ParsedVersion{}, err
	}

	switch versionType {
	case Major:
		parsedVersion.raiseMajor()
	case Minor:
		parsedVersion.raiseMinor()
	case Patch:
		parsedVersion.raisePatch()
	default:
		return ParsedVersion{}, fmt.Errorf("unexpected error occured during raising %v version", versionType)
	}

	parsedVersion.PreReleaseFields = []string{}
	parsedVersion.MetadataFields = []string{}

	return parsedVersion, nil
}
