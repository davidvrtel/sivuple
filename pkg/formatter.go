package pkg

import (
	"encoding/json"
	"fmt"
	"strings"

	"gopkg.in/yaml.v3"
)

func FormatOutput(parsedVersion ParsedVersion, outputType OutputType) (string, error) {
	var result string

	switch outputType {
	case Text:
		result = fmt.Sprintf("%v.%v.%v", parsedVersion.Major, parsedVersion.Minor, parsedVersion.Patch)

		if len(parsedVersion.PreReleaseFields) > 0 {
			result += fmt.Sprintf("-%v", strings.Join(parsedVersion.PreReleaseFields, "."))
		}

		if len(parsedVersion.MetadataFields) > 0 {
			result += fmt.Sprintf("+%v", strings.Join(parsedVersion.MetadataFields, "."))
		}
	case Json:
		// tmp, err := json.MarshalIndent(parsedVersion, "", "    ")
		tmp, err := json.Marshal(parsedVersion)

		if err != nil {
			return "", err
		}
		result = string(tmp)
	case Yaml:
		tmp, err := yaml.Marshal(parsedVersion)

		if err != nil {
			return "", err
		}

		result = string(tmp)
	}
	return result, nil
}
