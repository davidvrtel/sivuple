package pkg

import (
	"fmt"
	"strconv"
	"strings"
)

func ParseVersionType(arg string) (VersionType, error) {
	if err := validateVersionType(arg); err != nil {
		return "", err
	}
	versionType := coreVersionsTypeMap[strings.ToUpper(arg)]
	return versionType, nil
}

func ParseOutputType(arg string) (OutputType, error) {
	if err := validateOutputType(arg); err != nil {
		return "", err
	}
	outputType := outputTypeMap[strings.ToLower(arg)]
	return outputType, nil
}

// top level parser
func ParseVersion(version string) (ParsedVersion, error) {
	var split splitVersion
	var parsed ParsedVersion

	switch preReleasePosition, metadataPosition := strings.Index(version, "-"), strings.Index(version, "+"); {
	case preReleasePosition == -1 && metadataPosition == -1:
		split = splitVersion{
			core: version,
		}

		if err := validateCore(split.core); err != nil {
			return ParsedVersion{}, err
		}

	case preReleasePosition != -1 && metadataPosition == -1:
		split = splitVersion{
			core:          version[:preReleasePosition],
			hasPreRelease: true,
			preRelease:    version[preReleasePosition+1:],
		}

		if err := validateCore(split.core); err != nil {
			return ParsedVersion{}, err
		}
		if err := validatePreRelease(split.preRelease); err != nil {
			return ParsedVersion{}, err
		}
	case preReleasePosition != -1 && preReleasePosition < metadataPosition:
		split = splitVersion{
			core:          version[:preReleasePosition],
			hasPreRelease: true,
			preRelease:    version[preReleasePosition+1 : metadataPosition],
			hasMetadata:   true,
			metadata:      version[metadataPosition+1:],
		}

		if err := validateCore(split.core); err != nil {
			return ParsedVersion{}, err
		}
		if err := validatePreRelease(split.preRelease); err != nil {
			return ParsedVersion{}, err
		}
		if err := validateMetadata(split.metadata); err != nil {
			return ParsedVersion{}, err
		}

	case (preReleasePosition == -1 && metadataPosition != -1) || (preReleasePosition > metadataPosition):
		split = splitVersion{
			core:        version[:metadataPosition],
			hasMetadata: true,
			metadata:    version[metadataPosition+1:],
		}

		if err := validateCore(split.core); err != nil {
			return ParsedVersion{}, err
		}
		if err := validateMetadata(split.metadata); err != nil {
			return ParsedVersion{}, err
		}
	default:
		return ParsedVersion{}, fmt.Errorf("%v some temporary error", version)
	}

	core, err := parseCore(split.core)

	if err != nil {
		return ParsedVersion{}, err
	}

	parsed.Major = core[0]
	parsed.Minor = core[1]
	parsed.Patch = core[2]

	if split.hasPreRelease {
		parsed.PreReleaseFields = parsePreRelease(split.preRelease)
	}
	if split.hasMetadata {
		parsed.MetadataFields = parseMetadata(split.metadata)
	}

	return parsed, nil
}

// parse core of the version
func parseCore(version string) ([3]uint, error) {
	var core [3]uint
	stringArray := strings.Split(version, ".")

	for i, v := range stringArray {
		tmp, _ := strconv.ParseUint(v, 10, 0)
		core[i] = uint(tmp)
	}
	// core = [3]uint{uintArray[0], uintArray[1], uintArray[2]} //(*[3]uint)(uintArray)
	return core, nil
}

// parse pre release of the version
func parsePreRelease(version string) []string {
	return strings.Split(version, ".")
}

// parse metadata of the version
func parseMetadata(version string) []string {
	return strings.Split(version, ".")
}
