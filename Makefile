build:
	echo "Compiling for Windows and Linux amd64"
	GOOS=windows GOARCH=amd64 go build -o ./bin/sivuple.exe
	GOOS=linux GOARCH=amd64 go build -o ./bin/sivuple

dep:
	go mod download
