package main

import (
	"os"

	"gitlab.com/davidvrtel/sivuple/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
